package flash.net;


#if flash

import flash.utils.ByteArray;

extern class FileReference extends flash.events.EventDispatcher {
  var creationDate(default,null):Date;
  var creator(default,null):String;
  var data(default,null):ByteArray;
  var modificationDate(default,null):Date;
  var name(default,null):String;
  var size(default,null):Float;
  var type(default,null):String;
  function new();
  function browse(typeFilter:Array<FileFilter> = null):Bool;
  function cancel():Void;
  function download(request:URLRequest, defaultFileName:String = null):Void;
  function load():Void;
  function save(data:Dynamic, defaultFileName:String = null):Void;
  function upload(request:URLRequest, uploadDataFieldName:String = "Filedata", testUpload:Bool = false):Void;
}

#end
