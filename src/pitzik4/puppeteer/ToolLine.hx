/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.geom.Point;
import com.haxepunk.utils.Input;
import pitzik4.gfx.Cursor;

class ToolLine implements ITool {
  public var drawing:BitmapData;
  public var mouseDown:Bool;
  public var startX:Int = 0;
  public var startY:Int = 0;
  
  public function new() {
    mouseDown = false;
    startX = startY = 0;
  }
  
  public function renderOn(puppet:Puppet, mouseX:Int, mouseY:Int):Bool {
    if(mouseDown) {
      PDraw.line(startX, startY, mouseX, mouseY, puppet.editor.color, puppet.previewLayer.data);
      return true;
    } else {
      return puppet.previewLayer.setPixel(mouseX, mouseY, puppet.editor.color);
    }
  }
  public function renderFor(puppet:Puppet, target:BitmapData, camera:Point):Void {
    
  }
  public function update(mouseX:Int, mouseY:Int, width:Int, height:Int, color:Int, puppet:Puppet):Void {
    if(Input.mouseDown) {
      if(!mouseDown) {
        startX = mouseX;
        startY = mouseY;
      }
    } else if(mouseDown) {
      getDrawingSize(width, height);
      drawing.fillRect(drawing.rect, 0);
      PDraw.line(startX, startY, mouseX, mouseY, color, drawing);
      puppet.editedLayer.merge(drawing);
    }
    mouseDown = Input.mouseDown;
  }
  
  public function getDrawingSize(width:Int, height:Int):BitmapData {
    if(drawing == null) {
      drawing = new BitmapData(width, height, true, 0);
    } else if(drawing.width != width || drawing.height != height) {
      var temp:BitmapData = drawing;
      drawing = new BitmapData(width, height, false, 0);
      PDraw.drawImage(temp, 0, 0, drawing);
      temp.dispose();
    }
    return drawing;
  }
  
  public function doCursor(cursor:Cursor):Bool {
    return false;
  }
}
