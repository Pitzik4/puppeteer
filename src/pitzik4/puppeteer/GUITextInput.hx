/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import pitzik4.gfx.Cursor;
import pitzik4.gfx.SolidBox;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFieldType;
import com.haxepunk.HXP;
import openfl.Assets;

class GUITextInput extends GUIElement {
  public var usable(default, set):Bool;
  public function set_usable(val:Bool):Bool {
    if(usable == val) return usable;
    if(val) {
      bgBox.color = 0xFFFFFF;
    } else {
      bgBox.color = 0x999999;
    }
    return usable = val;
  }
  public var text(get, set):String;
  public inline function get_text():String {
    return field.text;
  }
  public function set_text(val:String):String {
    if(field.text == val) return val;
    return field.text = val;
  }
  public var bgBox:SolidBox;
  public var field:TextField;
  private var _zoom:Float;
  public var offsX:Float;
  public var offsY:Float;
  public var superior:GUIElement;
  
  public function new(x:Int, y:Int, width:Int, height:Int = 8, text:String = "", restrict:String = null, maxChars:Int = 0, layer:Int = -9) {
    super(x, y);
    layer = -10;
    offsX = x; offsY = y;
    _zoom = Main.scale;
    field = makeTextField(text, 8*_zoom, restrict, maxChars);
    field.width = width*_zoom; field.height = (height+2)*_zoom;
    this.width = width; this.height = height;
    this.layer = layer;
    graphic = bgBox = new SolidBox(0xFFFFFF, 0, 0, width, height);
    graphic.scrollX = graphic.scrollY = 0;
  }
  
  override public function doCursor(curs:Cursor, mx:Int = 0, my:Int = 0):Void {
    curs.hidden = false; curs.state = CursorState.TEXT;
  }
  
  override public function added():Void {
    super.added();
    HXP.stage.addChild(field);
  }
  override public function removed():Void {
    super.removed();
    HXP.stage.removeChild(field);
  }
  override public function render():Void {
    if(superior != null) {
      x = superior.x + offsX; y = superior.y + offsY;
    }
    super.render();
  }
  override public function update():Void {
    if(superior != null) {
      x = superior.x + offsX; y = superior.y + offsY;
      dead = dead || superior.dead;
    }
    if(Main.scale != _zoom) {
      _zoom = Main.scale;
      var textFormat:TextFormat = new TextFormat(Assets.getFont("font/04B_03__.ttf").fontName, 8*_zoom, 0xFF000000);
      field.setTextFormat(textFormat);
      field.defaultTextFormat = textFormat;
      field.width = width*_zoom; field.height = (height+2)*_zoom;
    }
    field.x = Math.floor(x)*_zoom; field.y = Math.floor(y)*_zoom;
    super.update();
  }
  override public function focus():Bool {
    if(superior != null) superior.focus();
    return true;
  }
  override public function unfocus():Void {
    super.unfocus();
    if(HXP.stage.focus == field) HXP.stage.focus = null;
  }
  
  public static function makeTextField(text:String = "", size:Float = 8, restrict:String = null, maxChars:Int = 0):TextField {
    var textFormat:TextFormat = new TextFormat(Assets.getFont("font/04B_03__.ttf").fontName, size, 0xFF000000);
    var out:TextField = new TextField();
    out.defaultTextFormat = textFormat;
    out.embedFonts = true;
    out.maxChars = maxChars;
    out.type = TextFieldType.INPUT;
    out.text = text;
    return out;
  }
}
