/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.Matrix;

interface ILayer {
  public var graphic(get,never):BitmapData;
  public var unique(get,never):Bool;
  public var layerWidth(get,never):Int;
  public var layerHeight(get,never):Int;
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void;
  public function resize(width:Int, height:Int):Void;
  public function merge(source:BitmapData, x:Float = 0, y:Float = 0):Void;
  public function eraseMerge(source:BitmapData, x:Float = 0, y:Float = 0):Void;
  public function draw(target:BitmapData, x:Float = 0, y:Float = 0):Void;
  public function clone():ILayer;
  public function getVersion():Int;
  public function setPixel(x:Int, y:Int, color:Int):Bool;
  public function getPixel(x:Int, y:Int):Int;
  public function floodFill(x:Int, y:Int, color:Int):Void;
}
