/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.Entity;
import flash.geom.Point;
import com.haxepunk.graphics.Graphiclist;
import flash.display.BitmapData;

class Puppet extends Entity {
  public var library:Array<Symbol>;
  public var frames:Array<Symbol>;
  public var frame(default, set):Int;
  public function set_frame(x:Int):Int {
    var y:Int = x % frames.length;
    if(x == -128) {
      frame = -1;
    } else if(x < 0) {
      while(x < 0) {
        x += frames.length;
      }
      frame = x;
    } else {
      frame = y;
    }
    if(frame >= 0 && edited != frames[frame])
      edited = frames[frame];
    return frame;
  }
  public var edited(default, set):Symbol;
  public function set_edited(x:Symbol):Symbol {
    dispSymInnerLayer.sym = x;
    var fr:Int = Lambda.indexOf(frames, x);
    fr = (fr<0)?-128:fr;
    if(frame != fr) frame = fr;
    edited = x;
    resizeView(edited.width, edited.height);
    editedLayer = edited.layers[0];
    return edited;
  }
  public var size:Point;
  public var imgWidth(default, set):Int;
  public function set_imgWidth(x:Int):Int {
    size.x = x;
    return imgWidth = x;
  }
  public var imgHeight(default, set):Int;
  public function set_imgHeight(x:Int):Int {
    size.y = x;
    return imgHeight = x;
  }
  public var scale(default, set):Float;
  public function set_scale(x:Float):Float {
    dispSymLayer.scaleX = dispSymLayer.scaleY = x;
    return scale = x;
  }
  public var editor:EditorScene;
  public var title:String;
  public var gl:Graphiclist;
  public var dispSym:Symbol;
  public var dispSymLayer:SymbolLayer;
  public var dispSymInnerLayer:SymbolLayer;
  public var previewLayer:BitmapLayer;
  public var editedLayer:ILayer;
  public var grid:GridLayer;
  
  public function new(editor:EditorScene, imgWidth:Int, imgHeight:Int, ?frames_:Array<BitmapData>, x:Int = 0, y:Int = 0, title:String = "Untitled") {
    super(x, y);
    if(frames_ != null && frames_.length > 0) {
      size = new Point(frames_[0].width, frames_[0].height);
      frames = [];
      for(fr in frames_) {
        if(fr.width == frames_[0].width && fr.height == frames_[0].height) {
          frames.push(new Symbol(fr));
        }
      }
    } else {
      size = new Point(imgWidth, imgHeight);
      frames = [new Symbol(size)];
    }
    this.imgWidth = imgWidth; this.imgHeight = imgHeight;
    this.editor = editor;
    this.title = title;
    library = [];
    graphic = gl = new Graphiclist();
    dispSym = new Symbol(size);
    previewLayer = cast(dispSym.makeEditableTop(), BitmapLayer);
    dispSym.addLayer(dispSymInnerLayer = new SymbolLayer(frames[0]), 1);
    dispSym.addLayer(grid = new GridLayer(imgWidth, imgHeight), 2);
    dispSymLayer = new SymbolLayer(dispSym);
    gl.add(dispSymLayer);
    scale = 1;
    frame = 0;
  }
  
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    if(frame >= 0) {
      expandImage(left, right, up, down);
    } else {
      edited.expand(left, right, up, down);
      resizeView(edited.width, edited.height);
    }
  }
  public function expandImage(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    imgWidth += left + right; imgHeight += up + down;
    for(f in frames) {
      f.expand(left, right, up, down);
    }
    if(frame >= 0) {
      resizeView(imgWidth, imgHeight);
    }
  }
  public function resizeView(?width:Null<Int>, ?height:Null<Int>):Void {
    var nwidth:Int = dispSym.width, nheight:Int = dispSym.height;
    if(width != null) {
      nwidth = width;
    }
    if(height != null) {
      nheight = height;
    }
    dispSym.resize(nwidth, nheight);
    dispSymInnerLayer.resize(nwidth+edited.originX, nheight+edited.originY);
    dispSymLayer.resize(Std.int(nwidth * scale + 0.5), Std.int(nheight * scale + 0.5));
    dispSymInnerLayer.x = edited.originX;
    dispSymInnerLayer.y = edited.originY;
  }
  public inline function isFrame(sym:Symbol):Bool {
    return Lambda.has(frames, sym);
  }
  public function addSymbol(sym:Symbol):Bool {
    if(Lambda.has(library, sym) || isFrame(sym)) return false;
    library.push(sym);
    return true;
  }
  override public function update():Void {
    super.update();
  }
}
