/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.geom.Point;
import com.haxepunk.utils.Input;
import pitzik4.gfx.Cursor;
import com.haxepunk.HXP;

class ToolEraser implements ITool {
  public var drawing:BitmapData;
  public var mouseDown:Bool;
  public var lastMouseX:Int;
  public var lastMouseY:Int;
  public var mxVis:Int;
  public var myVis:Int;
  
  public function new() {
    mouseDown = false;
    lastMouseX = lastMouseY = 0;
    mxVis = myVis = 0;
  }
  
  public function renderOn(puppet:Puppet, mouseX:Int, mouseY:Int):Bool {
    mxVis = mouseX; myVis = mouseY;
    if(mouseDown) {
      var grid:BitmapData = puppet.grid.graphic;
#if flash
      drawing.merge(grid, grid.rect, HXP.zero, 256, 256, 256, 0);
      puppet.previewLayer.merge(drawing);
#else
      grid.lock();
      for(x in 0...grid.width) {
        for(y in 0...grid.height) {
          if(drawing.getPixel32(x, y) & 0xFF000000 != 0xFF000000) {
            grid.setPixel32(x, y, 0);
          }
        }
      }
      grid.unlock();
      puppet.grid.dirty = true;
      puppet.previewLayer.merge(grid);
#end
      return true;
    }
    return false;
  }
  public function renderFor(puppet:Puppet, target:BitmapData, camera:Point):Void {
    var s:Int = Std.int(puppet.scale);
    var x:Int = Std.int(puppet.x + mxVis * s);
    var y:Int = Std.int(puppet.y + myVis * s);
    for(i in 0...s) {
      InvertCrosshairs.invertPixel(x+i, y-1, target);
      InvertCrosshairs.invertPixel(x+i, y+s, target);
      InvertCrosshairs.invertPixel(x-1, y+i, target);
      InvertCrosshairs.invertPixel(x+s, y+i, target);
    }
  }
  public function update(mouseX:Int, mouseY:Int, width:Int, height:Int, color:Int, puppet:Puppet):Void {
    mxVis = mouseX; myVis = mouseY;
    if(Input.mouseDown) {
      getDrawingSize(width, height);
      if(!mouseDown) {
        lastMouseX = mouseX;
        lastMouseY = mouseY;
        drawing.fillRect(drawing.rect, 0);
      }
      PDraw.line(lastMouseX, lastMouseY, mouseX, mouseY, 0xFFFFFFFF, drawing);
    } else if(mouseDown) {
      puppet.editedLayer.eraseMerge(drawing);
    }
    lastMouseX = mouseX;
    lastMouseY = mouseY;
    mouseDown = Input.mouseDown;
  }
  
  public function getDrawingSize(width:Int, height:Int):BitmapData {
    if(drawing == null) {
      drawing = new BitmapData(width, height, true, 0);
    } else if(drawing.width != width || drawing.height != height) {
      var temp:BitmapData = drawing;
      drawing = new BitmapData(width, height, false, 0);
      PDraw.drawImage(temp, 0, 0, drawing);
      temp.dispose();
    }
    return drawing;
  }
  
  public function doCursor(cursor:Cursor):Bool {
    cursor.hidden = true;
    return true;
  }
}
