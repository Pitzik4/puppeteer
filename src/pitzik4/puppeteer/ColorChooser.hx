/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import pitzik4.puppeteer.Colorator;
import pitzik4.gfx.SolidBox;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.geom.Point;
import com.haxepunk.graphics.Text;
import flash.text.TextFormatAlign;
import com.haxepunk.utils.Input;

class ColorChooser extends GUIWindow {
  public var ator(get, never):IColorator;
  public function get_ator():IColorator {
    if(atorID == 0) {
      return rgbator;
    } else {
      return hsvator;
    }
  }
  public var rgbator:IColorator;
  public var hsvator:IColorator;
  public var atorID(default, set):Int;
  public function set_atorID(val:Int):Int {
    if(atorID == val) return atorID;
    var col:Int = ator.toColor(facs);
    atorID = val;
    facs = ator.fromColor(col);
    return atorID;
  }
  public var facs(get, set):Array<Float>;
  public function get_facs():Array<Float> {
    var out:Array<Float> = [];
    for(s in sliders) {
      out.push(s.value);
    }
    return out;
  }
  public function set_facs(val:Array<Float>):Array<Float> {
    for(i in 0...val.length) {
      sliders[i].value = val[i];
    }
    return val;
  }
  public var sliders(default, set):Array<GUISlider>;
  public function set_sliders(val:Array<GUISlider>):Array<GUISlider> {
    for(i in 0...val.length) {
      val[i].superior = this;
      val[i].offsY = 22 + 10*i;
      val[i].offsX = 4;
    }
    height = 22 + 10*val.length + 4;
    return sliders = val;
  }
  public var tinputs(default, set):Array<GUITextInput>;
  public function set_tinputs(val:Array<GUITextInput>):Array<GUITextInput> {
    textVals = [];
    for(i in 0...val.length) {
      val[i].superior = this;
      val[i].offsY = 22 + 10*i;
      val[i].offsX = 108;
      textVals.push(val[i].text = Std.string(ator.transformAmt(facs[i], i)));
    }
    return tinputs = val;
  }
  public var oldFacs:Array<Float>;
  public var onModify:Int->Void;
  public var rgbLight:SolidBox;
  public var hsvLight:SolidBox;
  public var oldAtorID:Int = -1;
  public var textVals:Array<String>;
  
  public function new(color:Int, onModify:Int->Void = null, x:Float = 0, y:Float = 0) {
    super(128, 64, "Color picker", x, y);
    this.onModify = onModify;
    rgbator = new RGBColorator();
    hsvator = new HSVColorator();
    sliders = [new GUISlider(100), new GUISlider(100), new GUISlider(100)];
    facs = ator.fromColor(color);
    oldFacs = [-1,-1,-1];
    atorID = 0;
    tinputs = [new GUITextInput(0,0,16), new GUITextInput(0,0,16), new GUITextInput(0,0,16)];
    gl.add(rgbLight = new SolidBox(0xFF404040, 4, 10, 32, 10));
    gl.add(hsvLight = new SolidBox(0xFF404040, 40, 10, 32, 10));
    var t:Text;
    gl.add(t = new Text("RGB", 4, 10, 31, 9, {
      size: 8, align: TextFormatAlign.CENTER
      }));
    gl.add(t = new Text("HSV", 40, 10, 31, 9, {
      size: 8, align: TextFormatAlign.CENTER
      }));
  }
  override public function update():Void {
    for(i in 0...tinputs.length) {
      if(textVals[i] != tinputs[i].text) {
        var v:Null<Int> = Std.parseInt(tinputs[i].text);
        if(v == null) v = 0;
        sliders[i].value = Math.min(ator.unTransformAmt(v, i), 1);
      }
    }
    var facs = this.facs;
    var mod:Bool = false;
    if(atorID != oldAtorID || oldFacs.length != facs.length) {
      mod = true;
    } else {
      for(i in 0...facs.length) {
        if(oldFacs[i] != facs[i]) {
          mod = true; break;
        }
      }
    }
    if(mod) {
      oldFacs = facs;
      oldAtorID = atorID;
      if(onModify != null) onModify(ator.toColor(facs));
      for(i in 0...sliders.length) {
        sliders[i].bmp = genSliderImg(i, sliders[i].bmp);
      }
      for(i in 0...tinputs.length) {
        var v:Null<Int> = Std.parseInt(tinputs[i].text);
        var trns:Int = ator.transformAmt(facs[i], i);
        if(v != null && v != trns && ator.unTransformAmt(v, i) != facs[i])
          tinputs[i].text = Std.string(trns);
      }
    }
    var mx:Int = Input.mouseX;
    var my:Int = Input.mouseY;
    if(atorID != 0 || hsvLight.overlaps(mx, my, x, y)) {
      hsvLight.visible = true;
      if(Input.mousePressed) {
        atorID = 1;
      }
    } else {
      hsvLight.visible = false;
    }
    if(atorID == 0 || rgbLight.overlaps(mx, my, x, y)) {
      rgbLight.visible = true;
      if(Input.mousePressed) {
        atorID = 0;
      }
    } else {
      rgbLight.visible = false;
    }
    super.update();
    for(i in 0...tinputs.length) {
      textVals[i] = tinputs[i].text;
    }
  }
  override public function added():Void {
    for(s in sliders) {
      owner.addGUIElement(s);
    }
    for(t in tinputs) {
      owner.addGUIElement(t);
    }
  }
  override public function removed():Void {
    for(s in sliders) {
      owner.removeGUIElement(s);
    }
    for(t in tinputs) {
      owner.removeGUIElement(t);
    }
  }
  public function genSliderImg(fac:Int, bmp:BitmapData = null):BitmapData {
    if(bmp == null) {
      bmp = new BitmapData(sliders[0].width-2, sliders[0].height-2, false);
    }
    var s:Int = bmp.width-1;
    var facs = this.facs;
    for(i in 0...bmp.width) {
      facs[fac] = i/s;
      var col:Int = ator.toColor(facs);
      for(j in 0...bmp.height)
        bmp.setPixel(i, j, col);
    }
    return bmp;
  }
}
