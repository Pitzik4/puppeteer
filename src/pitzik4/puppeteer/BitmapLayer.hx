/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;
 
import flash.display.BitmapData;
import flash.display.Bitmap;
import com.haxepunk.Graphic;
import flash.geom.Point;
import flash.geom.Matrix;
 
class BitmapLayer extends Graphic implements ILayer {
  public var data:BitmapData;
  public var graphic(get,never):BitmapData;
  public function get_graphic():BitmapData {
    return data;
  }
  public var unique(get,never):Bool;
  public function get_unique():Bool {
    return true;
  }
  public var layerWidth(get,never):Int;
  public function get_layerWidth():Int {
    return data.width;
  }
  public var layerHeight(get,never):Int;
  public function get_layerHeight():Int {
    return data.height;
  }
  public var version:Int = 1;
  
  public function new(width:Int = 32, height:Int = 32, ?source:BitmapData, copy:Bool = true) {
    super();
    blit = true;
    if(source == null) {
      data = new BitmapData(width, height, true, 0);
    } else if(copy) {
      data = source.clone();
    } else {
      data = source;
    }
  }
  
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    var temp = data;
    data = new BitmapData(temp.width+left+right, temp.height+up+down, true, 0);
    PDraw.drawImage(temp, left, up, data);
    version++;
  }
  public function resize(width:Int, height:Int):Void {
    var temp = data;
    data = new BitmapData(width, height, true, 0);
    PDraw.drawImage(temp, 0, 0, data);
    version++;
  }
  public function merge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
    PDraw.drawImage(source, x, y, data);
    version++;
  }
  public function eraseMerge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
#if flash
    _point.setTo(x, y);
    data.threshold(source, source.rect, _point, "==", 0xFF000000, 0, 0xFF000000);
#else
    data.lock();
    var xi:Int = Std.int(x); var yi:Int = Std.int(y);
    for(xx in Main.imax(xi,0)...Main.imin(xi+source.width, data.width)) {
      for(yy in Main.imax(yi,0)...Main.imin(yi+source.height, data.height)) {
        if(source.getPixel32(xx-xi, yy-yi) & 0xFF000000 == 0xFF000000) {
          data.setPixel32(xx, yy, 0);
        }
      }
    }
    data.unlock();
#end
    version++;
  }
  public function setPixel(x:Int, y:Int, color:Int):Bool {
    if(SymbolLayer.setPixelIn(data, x, y, color)) {
      version++;
      return true;
    } else {
      return false;
    }
  }
  public function getPixel(x:Int, y:Int):Int {
    return SymbolLayer.getPixelFrom(data, x, y);
  }
  public function draw(target:BitmapData, x:Float = 0, y:Float = 0):Void {
    PDraw.drawImage(data, x, y, target);
  }
  override public function render(buffer:BitmapData, target:Point, camera:Point):Void {
    draw(buffer, target.x+x-camera.x*scrollX, target.y+y-camera.y*scrollY);
  }
  public function clone():BitmapLayer {
    return new BitmapLayer(0, 0, data);
  }
  public function getVersion():Int {
    return version;
  }
  public function floodFill(x:Int, y:Int, color:Int):Void {
    if(x >= 0 && y >= 0 && x < data.width && y < data.height) {
      data.floodFill(x, y, color);
      version++;
    }
  }
}
