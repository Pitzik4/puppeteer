/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

#if !html5

import flash.utils.ByteArray;
#if flash
import flash.net.FileReference;
import flash.display.PNGEncoderOptions;
#else
#if cpp
import cpp.vm.Thread;
#elseif neko
import neko.vm.Thread;
#end
import sys.io.Process;
import sys.io.File;
import sys.io.FileOutput;
#end
import flash.display.BitmapData;

class FileSaver {
#if flash
  public static var fileref:FileReference;
  
  public static function saveData(data:ByteArray):Void {
    if(fileref == null) fileref = new FileReference();
    fileref.save(data);
  }
#else
  public static function saveDialog(title:String=null, message:String=null, startDir:String=null):String {
    var args:Array<String> = ["save"];
    if(startDir != null) {
      if(message==null) message = "Save a file.";
    }
    if(message != null) {
      if(title==null) title = "Save";
    }
    for(a in [title, message, startDir]) if(a != null) args.push(a);
#if windows
    var p = new Process("Dialog.exe", args);
#else
    var p = new Process("./Dialog", args);
#end
    if(p.exitCode() == 0) {
      var out:String = p.stdout.readAll().toString();
      if(out.length > 0) {
        return out;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
  public static function saveData(data:ByteArray):Void {
    Thread.create(function():Void {
      var path:String = saveDialog();
      if(path != null) {
        var outfile:FileOutput = null;
        try {
          outfile = File.write(path, true);
          outfile.writeString(data.toString());
        } catch(e:Dynamic) {  }
        if(outfile!=null) outfile.close();
      }
    });
  }
#end
  public static inline function saveImage(image:BitmapData, format:String = "png"):Void {
#if flash
    saveData(image.encode(image.rect, new PNGEncoderOptions()));
#else
    saveData(image.encode(format));
#end
  }
}

#end
