/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.utils.Input;
import pitzik4.gfx.SolidBox;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.graphics.Stamp;
import flash.text.TextFormatAlign;

class ColorBoxes extends GUIElement {
  public var gl:Graphiclist;
  public var col1(default, set):Int;
  public function set_col1(x:Int):Int {
    x |= 0xFF000000;
    colorbox1.color = x;
    var text:String = StringTools.hex(x & 0xFFFFFF, 6);
    if(isBright(x)) {
      text = "<d>" + text + "</d>";
    }
    colorhex1.richText = text;
    return col1 = x;
  }
  public var col2(default, set):Int;
  public function set_col2(x:Int):Int {
    x |= 0xFF000000;
    colorbox2.color = x;
    var text:String = StringTools.hex(x & 0xFFFFFF, 6);
    if(isBright(x)) {
      text = "<d>" + text + "</d>";
    }
    colorhex2.richText = text;
    return col2 = x;
  }
  public var hoveredBox(default, set):Int;
  public function set_hoveredBox(x:Int):Int {
    if(x == 1) {
      colorborder1.play(overAnim);
      colorborder2.play("normal");
    } else if(x == 2) {
      colorborder1.play("normal");
      colorborder2.play(overAnim);
    } else {
      colorborder1.play("normal");
      colorborder2.play("normal");
    }
    return hoveredBox = x;
  }
  public var overAnim(get, never):String;
  public function get_overAnim():String {
    return Input.mouseDown ? "click" : "hover";
  }
  public var fromBottom:Int;
  public var fromLeft:Int;
  public var colorbox1:SolidBox;
  public var colorbox2:SolidBox;
  public var colorborder1:Spritemap;
  public var colorborder2:Spritemap;
  public var colorhex1:Text;
  public var colorhex2:Text;
  public var editor:EditorScene;
  public var cchooser:ColorChooser;
  
  public function new(editor:EditorScene, col1:Int = 0xFF000000, col2:Int = 0xFFFFFFFF, fromBottom:Int = 4, fromLeft:Int = 4) {
    super();
    layer = -4;
    this.editor = editor;
    colorbox1 = new SolidBox(col1, 2, 2, 28, 28);
    colorbox2 = new SolidBox(col2, 18, 18, 28, 28);
    colorhex1 = new Text(StringTools.hex(col1 & 0xFFFFFF, 6), 0, 12, 32, 6, {
      font: "font/hextiny/Hex-Tiny.ttf", size: 8, align: TextFormatAlign.CENTER});
    colorhex1.addStyle("d", { color: 0 });
    colorhex2 = new Text(StringTools.hex(col2 & 0xFFFFFF, 6), 16, 32, 32, 6, {
      font: "font/hextiny/Hex-Tiny.ttf", size: 8, align: TextFormatAlign.CENTER});
    colorhex2.addStyle("d", { color: 0 });
    this.col1 = col1;
    this.col2 = col2;
    this.fromBottom = fromBottom;
    this.fromLeft = fromLeft;
    colorborder1 = new Spritemap("graphics/colorbox.png", 32, 32);
    colorborder1.add("normal", [0]);
    colorborder1.add("hover", [1]);
    colorborder1.add("click", [2]);
    colorborder2 = new Spritemap("graphics/colorbox.png", 32, 32);
    colorborder2.add("normal", [0]);
    colorborder2.add("hover", [1]);
    colorborder2.add("click", [2]);
    colorborder2.x = colorborder2.y = 16;
    graphic = gl = new Graphiclist();
    gl.add(colorbox2); gl.add(colorborder2);
    gl.add(colorbox1); gl.add(colorborder1);
    gl.add(colorhex1); gl.add(colorhex2);
    gl.add(new Stamp("graphics/fgbgswap.png", 34, 2));
    width = height = 48;
    x = fromLeft; y = HXP.height - fromBottom - height;
  }
  override public function update():Void {
    x = fromLeft; y = HXP.height - fromBottom - height;
    var mx:Int = Input.mouseX; var my:Int = Input.mouseY;
    if(mouseOver) {
      if(box1Overlaps(mx, my)) {
        hoveredBox = 1;
        if(Input.mousePressed) {
          if(cchooser != null) cchooser.dead = true;
          cchooser = new ColorChooser(col1, setColor1, x, y-64);
          if(owner != null) owner.addGUIElement(cchooser);
        }
      } else if(box2Overlaps(mx, my)) {
        hoveredBox = 2;
        if(Input.mousePressed) {
          if(cchooser != null) cchooser.dead = true;
          cchooser = new ColorChooser(col2, setColor2, x, y-64);
          if(owner != null) owner.addGUIElement(cchooser);
        }
      } else {
        hoveredBox = 0;
        if(Input.mousePressed && swapperOverlaps(mx, my)) {
          var temp:Int = col1;
          col1 = col2; col2 = temp;
        }
      }
    } else {
      hoveredBox = 0;
    }
    super.update();
  }
  override public function overlaps(mx:Int, my:Int):Bool {
    return mx >= x && mx < x+width && my >= y && my < y+height && (box1Overlaps(mx, my) || box2Overlaps(mx, my) || swapperOverlaps(mx, my));
  }
  public inline function box1Overlaps(mx:Int, my:Int):Bool {
    return mx < x+32 && my < y+32;
  }
  public inline function box2Overlaps(mx:Int, my:Int):Bool {
    return mx >= x+16 && my >= y+16;
  }
  public inline function swapperOverlaps(mx:Int, my:Int):Bool {
    return mx >= x+34 && mx < x+46 && my >= y+2 && my < y+14;
  }
  public function setColor1(col:Int):Void {
    col1 = col;
  }
  public function setColor2(col:Int):Void {
    col2 = col;
  }
  public static inline function isBright(x:Int):Bool {
    return x & 0xFF0000 > 0x800000 || x & 0xFF00 > 0x8000 || x & 0xFF > 0x80;
  }
}
