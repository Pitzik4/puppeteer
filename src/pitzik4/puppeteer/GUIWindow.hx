/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.graphics.Graphiclist;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.graphics.Text;
import com.haxepunk.utils.Input;
import flash.display.BitmapData;
import flash.geom.Point;

class GUIWindow extends GUIElement {
  public var gl:Graphiclist;
  public var window:WindowGraphic;
  public var title:Text;
  public var grabX:Int = -1;
  public var grabY:Int = -1;
  public var realLayer:Int;
  public var tempLayerOffs:Int = 0;
  public var xbutton:Spritemap;
  
  public function new(width:Int = 200, height:Int = 100, title:String = "", x:Float = 0, y:Float = 0) {
    super(x, y);
    layer = realLayer = -8;
    this.width = width; this.height = height;
    graphic = gl = new Graphiclist();
    gl.scrollX = gl.scrollY = 0;
    gl.add(window = new WindowGraphic(width, height));
    window.scrollX = window.scrollY = 0;
    gl.add(this.title = new Text(title, 2, 0, width-4, 16, {
      size: 8, wordWrap: false, resizable: false
      }));
    this.title.scrollX = this.title.scrollY = 0;
    gl.add(xbutton = new Spritemap("graphics/xbutton.png", 8, 8));
    xbutton.scrollX = xbutton.scrollY = 0;
    xbutton.x = width-8;
    xbutton.add("normal", [0]);
    xbutton.add("hover", [1]);
    xbutton.add("click", [2]);
  }
  override public function render():Void {
    window.width = width; window.height = height;
    super.render();
  }
  override public function update():Void {
    xbutton.x = width-8;
    layer = realLayer;
    if(mouseOver && Input.mouseX >= x+width-8 && Input.mouseY < y+8) {
      xbutton.play("hover");
      if(Input.mousePressed) {
        dead = true;
      }
    } else {
      xbutton.play("normal");
    }
    if(Input.mouseDown) {
      if(Input.mousePressed && mouseOver && Input.mouseX>=x&&Input.mouseX<x+width&&Input.mouseY>=y&&Input.mouseY<y+9) {
        grabX = Std.int(Input.mouseX - x); grabY = Std.int(Input.mouseY - y);
      } else if(grabX >= 0 && grabY >= 0) {
        x = Input.mouseX - grabX; y = Input.mouseY - grabY;
      }
    } else {
      grabX = grabY = -1;
    }
    layer += tempLayerOffs;
    tempLayerOffs = 0;
    super.update();
  }
  override public function overlaps(mx:Int, my:Int):Bool {
    if(grabX >= 0 && grabY >= 0) return true;
    return super.overlaps(mx, my);
  }
  override public function focus():Bool {
    tempLayerOffs = -1;
    return true;
  }
}
class WindowGraphic extends Graphic {
  public var width:Int;
  public var height:Int;
  public var outlineColor:Int = 0x606060;
  public var titleOutlineColor:Int = 0x204080;
  public var titleColor:Int = 0x4080FF;
  public var titleShadowColor:Int = 0x3465D0;
  public var bgColor:Int = 0xC0C0C0;
  public var bgShadowColor:Int = 0xA0A0A0;
  
  public function new(width:Int = 200, height:Int = 100) {
    super();
    blit = true;
    this.width = width; this.height = height;
  }
  
  override public function render(target:BitmapData, point:Point, camera:Point):Void {
    var ox:Int = Std.int(x+point.x-camera.x*scrollX);
    var oy:Int = Std.int(y+point.y-camera.y*scrollY);
    PDraw.line(ox+1, oy, ox+width-2, oy, titleOutlineColor, target);
    PDraw.line(ox+1, oy+height-1, ox+width-2, oy+height-1, outlineColor, target);
    PDraw.line(ox, oy+1, ox, oy+8, titleOutlineColor, target);
    PDraw.line(ox+width-1, oy+1, ox+width-1, oy+8, titleOutlineColor, target);
    PDraw.line(ox, oy+9, ox, oy+height-2, outlineColor, target);
    PDraw.line(ox+width-1, oy+9, ox+width-1, oy+height-2, outlineColor, target);
    PDraw.fillRect(ox+1, oy+1, width-3, 8, titleColor, target);
    PDraw.fillRect(ox+1, oy+9, width-3, height-11, bgColor, target);
    PDraw.line(ox+width-2, oy+1, ox+width-2, oy+8, titleShadowColor, target);
    PDraw.line(ox+width-2, oy+9, ox+width-2, oy+height-2, bgShadowColor, target);
    PDraw.line(ox+1, oy+height-2, ox+width-3, oy+height-2, bgShadowColor, target);
  }
}
