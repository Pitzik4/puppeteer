/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.RenderMode;
import com.haxepunk.Engine;
import com.haxepunk.HXP;
import flash.geom.Rectangle;

class Main extends Engine {
  public static inline var DEF_SCALE:Float = 2.0;
  public static var scale:Float = DEF_SCALE;
  
  private var _rect:Rectangle;
  
  public function new() {
    super(0, 0, 60, false, RenderMode.BUFFER);
    _rect = new Rectangle();
  }
  override public function init():Void {
#if debug
    HXP.console.enable();
#end
    HXP.scene = new EditorScene();
    resize();
  }
  
  public static function main():Void {
    new Main();
  }
  override public function resize():Void {
    var w:Int = HXP.stage.stageWidth;
    var h:Int = HXP.stage.stageHeight;
    if(HXP.width == 0) HXP.width = w;
    if(HXP.height == 0) HXP.height = h;
    HXP.windowWidth = w;
    HXP.windowHeight = h;
    HXP.screen.scale = scale;
    HXP.resize(w, h);
  }
  public static inline function imin(a:Int, b:Int):Int {
    return (a>b)?b:a;
  }
  public static inline function imax(a:Int, b:Int):Int {
    return (a>b)?a:b;
  }
  public static inline function iclamp(val:Int, min:Int, max:Int):Int {
    return imin(imax(val, min), max);
  }
}
