/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.HXP;

interface IColorator {
  public var name(get,never):String;
  public var factors(get,never):Array<String>;
  public var allowedChars(get, never):String;
  public function toColor(facs:Array<Float>):Int;
  public function fromColor(col:Int):Array<Float>;
  public function transformAmt(fac:Float, which:Int = 0):Int;
  public function fromString(str:String, which:Int = 0):Float;
  public function unTransformAmt(val:Int, which:Int = 0):Float;
}
class RGBColorator implements IColorator {
  public var name(get,never):String;
  public function get_name():String { return "RGB"; }
  public var factors(get,never):Array<String>;
  public function get_factors():Array<String> { return ["Red","Grn","Blu"]; }
  public var allowedChars(get, never):String;
  public function get_allowedChars():String { return "0-9"; }
  public static inline function to255(x:Float) {
    return (Std.int(x*255));
  }
  public function toColor(facs:Array<Float>):Int {
    return HXP.getColorRGB(to255(facs[0]), to255(facs[1]), to255(facs[2]));
  }
  public function fromColor(col:Int):Array<Float> {
    return [HXP.getRed(col)/255, HXP.getGreen(col)/255, HXP.getBlue(col)/255];
  }
  public function transformAmt(fac:Float, which:Int = 0):Int {
    return to255(fac);
  }
  public function unTransformAmt(val:Int, which:Int = 0):Float {
    return val / 255;
  }
  public function fromString(str:String, which:Int = 0):Float {
    return Std.parseInt(str) / 255;
  }
  public function new() {  }
}
class HSVColorator implements IColorator {
  public var name(get,never):String;
  public function get_name():String { return "HSV"; }
  public var factors(get,never):Array<String>;
  public function get_factors():Array<String> { return ["Hue","Sat","Val"]; }
  public var allowedChars(get, never):String;
  public function get_allowedChars():String { return "0-9"; }
  public function toColor(facs:Array<Float>):Int {
    return HXP.getColorHSV(facs[0], facs[1], facs[2]);
  }
  public function fromColor(col:Int):Array<Float> {
    return [HXP.getColorHue(col), HXP.getColorSaturation(col), HXP.getColorValue(col)];
  }
  public function transformAmt(fac:Float, which:Int = 0):Int {
    return Std.int((which==0)?fac*360:fac*100);
  }
  public function unTransformAmt(val:Int, which:Int = 0):Float {
    return (which==0)?val/360:val/100;
  }
  public function fromString(str:String, which:Int = 0):Float {
    return Std.parseInt(str) / (which==0?360:100);
  }
  public function new() {  }
}
