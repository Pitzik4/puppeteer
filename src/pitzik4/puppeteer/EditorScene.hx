/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.Scene;
import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import haxe.ds.ArraySort;
import pitzik4.gfx.SolidBox;
import pitzik4.gfx.BGColor;
import pitzik4.gfx.Cursor;
import flash.display.BitmapData;
import openfl.Assets;
import flash.ui.Mouse;

class EditorScene extends Scene {
  public var puppets:List<Puppet>;
  public var puppet(default, set):Puppet;
  public function set_puppet(x:Puppet):Puppet {
    if(this.puppet != null)
      remove(this.puppet);
    add(x);
    return this.puppet = x;
  }
  public var tool:ITool;
  public var color(get, set):Int;
  public inline function get_color():Int {
    return colorBoxes.col1;
  }
  public inline function set_color(x:Int):Int {
    return colorBoxes.col1 = x;
  }
  public var color2(get, set):Int;
  public inline function get_color2():Int {
    return colorBoxes.col2;
  }
  public inline function set_color2(x:Int):Int {
    return colorBoxes.col2 = x;
  }
  public var updatedLast:Bool = false;
  public var cursor:Cursor;
  public var guiElements:Array<GUIElement>;
  public var colorBoxes:ColorBoxes;
  public var focus:GUIElement = null;
  
  override public function begin():Void {
    HXP.stage.color = 0x333333;
    addGraphic(new BGColor(), 1024);
    add(cursor = new Cursor("graphics/cursor.png"));
    guiElements = [];
    puppets = new List<Puppet>();
    puppets.push(puppet = new Puppet(this, 32, 32, [Assets.getBitmapData("graphics/meddler.png"), PDraw.genCircle(32)]));
    //puppet.addSymbol(new Symbol(Assets.getBitmapData("graphics/hardhat.png")));
    //var l:SymbolLayer = new SymbolLayer(puppet.library[0], 0, 0, 0, 0, 32, 32);
    //l.angle = -22.5;
    //l.scaleX = l.scaleY = 1;
    //puppet.edited.addLayer(l);
    //puppet.editedLayer = l;
    //PDraw.ellipse(0, 0, 32, 32, 0xFFFFFFFF, puppet.frames[0].layers[0].graphic);
    puppet.x = puppet.y = 48;
    puppet.scale = 2;
    //puppet.edited = sym;
    tool = new ToolBrush();
    addGUIElement(colorBoxes = new ColorBoxes(this));
    Mouse.hide();
  }
  override public function render():Void {
    var buf:BitmapData = puppet.previewLayer.data;
    if(updatedLast) {
      buf.fillRect(buf.rect, 0);
      puppet.previewLayer.version++;
    }
    if(tool != null) {
      updatedLast = tool.renderOn(puppet, getMouseX(), getMouseY());
    } else updatedLast = false;
    super.render();
    if(tool != null)
      tool.renderFor(puppet, HXP.buffer, HXP.camera);
  }
  override public function update():Void {
    ArraySort.sort(guiElements, layerCompare);
    var ok:Bool = true;
    var i:Int = 0;
    while(i < guiElements.length) {
      var el = guiElements[i];
      if(el.dead) {
        removeGUIElement(el);
        continue;
      }
      if(el.overlaps(Input.mouseX, Input.mouseY) && ok) {
        el.doCursor(cursor, Input.mouseX, Input.mouseY);
        el.mouseOver = true;
        ok = false;
        if(Input.mousePressed && el != focus && el.focus()) {
          if(focus != null) focus.unfocus();
          focus = el;
        }
      }
      i++;
    }
    if(ok) {
      if(tool == null || !tool.doCursor(cursor)) {
        cursor.hidden = false;
        cursor.state = CursorState.CROSSHAIR;
      }
      if(Input.mousePressed && focus != null) {
        focus.unfocus();
        focus = null;
      }
    }
    if(ok && tool != null)
      tool.update(getMouseX(), getMouseY(), puppet.edited.width, puppet.edited.height, color, puppet);
    if(Input.pressed(Key.B)) {
      tool = new ToolBrush();
    } else if(Input.pressed(Key.I)) {
      tool = new ToolDropper();
    } else if(Input.pressed(Key.G)) {
      tool = new ToolFill();
    } else if(Input.pressed(Key.E)) {
      tool = new ToolEraser();
    } else if(Input.pressed(Key.L)) {
      tool = new ToolLine();
    }
    if(Input.mouseWheel) {
      puppet.scale += Input.mouseWheelDelta;
      if(puppet.scale < 1) puppet.scale = 1;
      while(puppet.scale*Main.imax(puppet.imgWidth,puppet.imgHeight) > HXP.height) puppet.scale--;
    }
    super.update();
  }
  public static function layerCompare(x:Entity, y:Entity):Int {
    return x.layer - y.layer;
  }
  public inline function getMouseX():Int {
    return Std.int((Input.mouseX+HXP.camera.x-puppet.x)/puppet.scale);
  }
  public inline function getMouseY():Int {
    return Std.int((Input.mouseY+HXP.camera.y-puppet.y)/puppet.scale);
  }
  public function addGUIElement(el:GUIElement):EditorScene {
    el.owner = this;
    guiElements.push(el);
    add(el);
    return this;
  }
  public function removeGUIElement(el:GUIElement):EditorScene {
    guiElements.remove(el);
    remove(el);
    return this;
  }
}
