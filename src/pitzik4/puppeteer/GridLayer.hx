/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.geom.Point;
import com.haxepunk.Graphic;

class GridLayer extends Graphic implements ILayer {
  public static inline var COLOR_1:Int = 0x999999;
  public static inline var COLOR_2:Int = 0x666666;
  
  public var graphic(get,never):BitmapData;
  public function get_graphic():BitmapData {
    if(gfc.width != width || gfc.height != height) {
      gfc.dispose();
      gfc = regenGFC();
    }
    if(dirty) {
      draw(gfc);
      dirty = false;
    }
    return gfc;
  }
  public var unique(get,never):Bool;
  public function get_unique():Bool {
    return false;
  }
  public var width:Int;
  public var height:Int;
  public var layerWidth(get,never):Int;
  public function get_layerWidth():Int {
    return width;
  }
  public var layerHeight(get,never):Int;
  public function get_layerHeight():Int {
    return height;
  }
  public var size:Int;
  public var col1:Int;
  public var col2:Int;
  public var gfc:BitmapData;
  private var _rect:Rectangle;
  public var dirty:Bool = false;
  
  public function new(width:Int, height:Int, size:Int = 8, col1:Int = COLOR_1, col2:Int = COLOR_2) {
    super();
    _rect = new Rectangle();
    this.width = width; this.height = height;
    this.size = size;
    this.col1 = col1; this.col2 = col2;
    gfc = regenGFC();
  }
  
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    width += left + right;
    height += up + down;
  }
  public function resize(width:Int, height:Int):Void {
    this.width = width; this.height = height;
  }
  public function merge(source:BitmapData, x:Float = 0, y:Float = 0):Void {  }
  public function eraseMerge(source:BitmapData, x:Float = 0, y:Float = 0):Void {  }
  public function draw(target:BitmapData, x:Float = 0, y:Float = 0):Void {
    for(xx in 0...Std.int(Math.ceil(width/size))) {
      for(yy in 0...Std.int(Math.ceil(height/size))) {
        _rect.setTo(x + xx*size, y + yy*size, Math.min(size, width - xx*size), Math.min(size, height - yy*size));
        target.fillRect(_rect, 0xFF000000 | (((xx+yy)%2==0)?col1:col2));
      }
    }
  }
  override public function render(target:BitmapData, point:Point, camera:Point):Void {
    draw(target, x + point.x - camera.x*scrollX, y + point.y - camera.y*scrollY);
  }
  public function clone():GridLayer {
    return new GridLayer(width, height, size, col1, col2);
  }
  public function getVersion():Int {
    return 1;
  }
  public function setPixel(x:Int, y:Int, color:Int):Bool { return false; }
  public function getPixel(x:Int, y:Int):Int {
    if((Std.int(x/size)+Std.int(y/size)) % 2 == 0) {
      return col1;
    } else {
      return col2;
    }
  }
  public function floodFill(x:Int, y:Int, color:Int):Void {  }
  
  public function regenGFC():BitmapData {
    var out:BitmapData = new BitmapData(width, height);
    draw(out);
    return out;
  }
}
