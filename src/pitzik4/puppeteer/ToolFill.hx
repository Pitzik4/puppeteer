/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.geom.Point;
import com.haxepunk.utils.Input;
import pitzik4.gfx.Cursor;

class ToolFill implements ITool {
  public function new() {
    
  }
  
  public function renderOn(puppet:Puppet, mouseX:Int, mouseY:Int):Bool {
    return puppet.previewLayer.setPixel(mouseX, mouseY, puppet.editor.color);
  }
  public function renderFor(puppet:Puppet, target:BitmapData, camera:Point):Void {
    
  }
  public function update(mouseX:Int, mouseY:Int, width:Int, height:Int, color:Int, puppet:Puppet):Void {
    if(Input.mousePressed) {
      puppet.editedLayer.floodFill(mouseX, mouseY, color);
    }
  }
  
  public function doCursor(cursor:Cursor):Bool {
    return false;
  }
}
