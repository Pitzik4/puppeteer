/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.Graphic;
import flash.display.BitmapData;
import flash.geom.Point;
import com.haxepunk.utils.Input;

class GUISlider extends GUIElement {
  public var superior:GUIElement;
  // Between 0 and 1, inclusive
  public var value:Float;
  public var offsX:Float;
  public var offsY:Float;
  public var clicked:Bool;
  public var bmp:BitmapData;
  
  public function new(width:Int, height:Int = 8, x:Float = 0, y:Float = 0, doBMP:Bool = true) {
    super(x, y);
    if(doBMP)
      bmp = new BitmapData(width-2, height-2, false);
    layer = -10;
    offsX = x; offsY = y;
    this.width = width; this.height = height;
    graphic = new SliderGraphic(this);
    graphic.scrollX = graphic.scrollY = 0;
  }
  override public function render():Void {
    if(superior != null) {
      x = superior.x + offsX;
      y = superior.y + offsY;
    }
    super.render();
  }
  override public function update():Void {
    if(superior != null) {
      x = superior.x + offsX;
      y = superior.y + offsY;
      layer = superior.layer;
    }
    if(mouseOver && Input.mousePressed) {
      clicked = true;
    } else if(!Input.mouseDown) {
      clicked = false;
    }
    if(clicked) {
      value = (Input.mouseX-x-1)/(width-3);
      value = (value<0)?0:((value>1)?1:value);
    }
    super.update();
  }
  override public function focus():Bool {
    superior.focus();
    return super.focus();
  }
}
class SliderGraphic extends Graphic {
  public var slider:GUISlider;
  
  public function new(slider:GUISlider) {
    super();
    this.slider = slider;
    blit = true;
  }
  override public function render(target:BitmapData, point:Point, camera:Point) {
    var xp:Int = Std.int(x+point.x-camera.x*scrollX);
    var yp:Int = Std.int(y+point.y-camera.y*scrollY);
    if(slider.bmp != null) {
      PDraw.drawImage(slider.bmp, xp+1, yp+1, target);
    }
    PDraw.rect(xp, yp, slider.width, slider.height, 0xFFFFFFFF, 1, target);
    var sx:Int = Std.int(xp+slider.value*(slider.width-3))+1;
    PDraw.line(sx, yp, sx, yp+slider.height-1, 0xFFFFFFFF, target);
  }
}
