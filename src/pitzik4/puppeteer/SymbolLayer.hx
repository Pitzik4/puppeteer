/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import flash.geom.Point;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.display.Bitmap;
import com.haxepunk.graphics.Image;

class SymbolLayerState {
  public var sym:Symbol;
  public var scaleX:Float;
  public var scaleY:Float;
  public var angle:Float;
  public var symX:Float;
  public var symY:Float;
  public var symvers:Int;
  public function new(sym:Symbol = null, symX:Float = 0, symY:Float = 0, scaleX:Float = 1, scaleY:Float = 1, angle:Float = 0, symvers:Int = 0) {
    setTo(sym, symX, symY, scaleX, scaleY, angle, symvers);
  }
  public function setTo(sym:Symbol = null, symX:Float = 0, symY:Float = 0, scaleX:Float = 1, scaleY:Float = 1, angle:Float = 0, symvers:Int = 0):SymbolLayerState {
    this.sym = sym;
    this.symX = symX;
    this.symY = symY;
    this.scaleX = scaleX;
    this.scaleY = scaleY;
    this.angle = angle;
    this.symvers = symvers;
    return this;
  }
  public function capture(syml:SymbolLayer):SymbolLayerState {
    return setTo(syml.sym, syml.symX, syml.symY, syml.scaleX, syml.scaleY, syml.angle, syml.sym.getVersion());
  }
  public function copyFrom(symls:SymbolLayerState):SymbolLayerState {
    return setTo(symls.sym, symls.symX, symls.symY, symls.scaleX, symls.scaleY, symls.angle, symls.symvers);
  }
  public function equals(other:SymbolLayerState):Bool {
    return sym == other.sym && symX == other.symX && symY == other.symY && scaleX == other.scaleX && scaleY == other.scaleY && angle == other.angle && symvers == other.symvers;
  }
  public function unchanged(syml:SymbolLayer):Bool {
    return sym == syml.sym && symX == syml.symX && symY == syml.symY && scaleX == syml.scaleX && scaleY == syml.scaleY && angle == syml.angle && symvers == syml.sym.getVersion();
  }
}
class SymbolLayer extends Graphic implements ILayer {
  public var graphic(get,never):BitmapData;
  public function get_graphic():BitmapData {
    return getDrawn();
  }
  public var unique(get,never):Bool;
  public function get_unique():Bool {
    return false;
  }
  public var sym:Symbol;
  private var _rendered:BitmapData;
  private var _width:Int;
  private var _height:Int;
  public var layerWidth(get,never):Int;
  public function get_layerWidth():Int {
    return _width;
  }
  public var layerHeight(get,never):Int;
  public function get_layerHeight():Int {
    return _height;
  }
  private var _point2:Point;
  private var _point3:Point;
  private var _matrix:Matrix;
  private var _lastRender:SymbolLayerState;
  private var _version:Int = 0;
  private var _bmp:BitmapData;
  public var scaleX:Float;
  public var scaleY:Float;
  public var angle:Float;
  public var symX:Float;
  public var symY:Float;
  private static var _spoint:Point;
  private static var _srect:Rectangle;
  
  public function new(sym:Symbol, symX:Float = 0, symY:Float = 0, x:Float = 0, y:Float = 0, width:Int = -1, height:Int = -1) {
    super();
    this.sym = sym;
    scaleX = scaleY = 1;
    angle = 0;
    _point = new Point();
    _point2 = new Point();
    _point3 = new Point();
    _matrix = new Matrix();
    blit = true;
    this.sym = sym;
    this.x = x; this.y = y;
    this.symX = symX; this.symY = symY;
    _width = (width<0)?Std.int(sym.width+symX+0.5):width;
    _height = (height<0)?Std.int(sym.height+symY+0.5):height;
    _rendered = new BitmapData(_width, _height, true);
    _lastRender = new SymbolLayerState();
  }
  
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    symX += left; symY += up;
    _width += left + right;
    _height += up + down;
  }
  public function resize(width:Int, height:Int):Void {
    _width = width;
    _height = height;
  }
  public function merge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
    //var angle:Float = angle % 360;
    if(getSimpleRender()) {
      sym.merge(source, x - symX, y - symY);
    } else {
      _matrix = getMatrix();
      _matrix.invert();
      _matrix.translate(x, y);
      /*if(angle == 0) {
        _bitmap.bitmapData = source;
        sym.specialMerge(_bitmap, _matrix);
      } else {*/
      var top:BitmapLayer = sym.makeEditableTop();
      complexMatrixDraw(source, top.data, _matrix);
      top.version++;
      //}
    }
  }
  public function eraseMerge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
    if(getSimpleRender()) {
      sym.eraseMerge(source, x - symX, y - symY);
    } else {
      var bmp:BitmapData = getBitmapData(_width, _height);
      _matrix = getMatrix();
      _matrix.invert();
      _matrix.translate(x, y);
      complexMatrixDraw(source, bmp, _matrix);
      sym.eraseMerge(bmp, -symX, -symY);
    }
  }
  public function getBitmapData(width:Int, height:Int):BitmapData {
    if(_bmp == null || _bmp.width != width || _bmp.height != height) {
      _bmp = new BitmapData(width, height, true, 0);
    }
    return _bmp;
  }
  public function drawB(target:BitmapData, x:Float = 0, y:Float = 0, forceRedraw:Bool = false):Void {
    _point.setTo(x, y);
    if(forceRedraw) {
      renderB(target, _point, HXP.zero);
    } else {
      PDraw.drawImage(getDrawn(), x, y, target);
    }
  }
  public function draw(target:BitmapData, x:Float = 0, y:Float = 0):Void {
    drawB(target, x, y);
  }
  override public function render(target:BitmapData, point:Point, camera:Point):Void {
    PDraw.drawImage(getDrawn(), point.x+x - camera.x*scrollX,
        point.y+y - camera.y*scrollY, target);
  }
  public function renderB(target:BitmapData, point:Point, camera:Point):Void {
    //draw(target, point.x-camera.x*scrollX, point.y-camera.y*scrollY);
    // determine drawing location
    var angle:Float = this.angle % 360;
    _point3.x = point.x + x + symX - sym.originX - camera.x * scrollX;
    _point3.y = point.y + y + symY - sym.originY - camera.y * scrollY;
    
    var bmp:BitmapData = sym.rendered();
    if(getSimpleRender()) {
      target.copyPixels(bmp, bmp.rect, _point3, null, null, true);
    } else {
      // render with transformation
      _matrix = getMatrix();
      if(angle != 0 || true) {
        complexMatrixDraw(bmp, target, _matrix);
      } else {
        target.draw(bmp, _matrix, null, null, null, false);
      }
    }
  }
  public static function justTranslateScale(matrix:Matrix):JustTranslateScale {
    if(_spoint == null) _spoint = new Point();
    _spoint.setTo(0, 0);
    transformSPoint(matrix);
    var x1:Float = _spoint.x, y1:Float = _spoint.y;
    _spoint.setTo(1, 0);
    transformSPoint(matrix);
    var x2:Float = _spoint.x, y2:Float = _spoint.y;
    if(y2 - y1 != 0) return new JustTranslateScale();
    _spoint.setTo(0, 1);
    transformSPoint(matrix);
    var x3:Float = _spoint.x, y3:Float = _spoint.y;
    if(x3 - x1 != 0) return new JustTranslateScale();
    return new JustTranslateScale(true, x2-x1, y3-y1, x1, y1);
  }
  public static function translateScaleDraw(source:BitmapData, target:BitmapData, jts:JustTranslateScale, matrix:Matrix):Bool {
    if(!jts.yes) return false;
    target.draw(source, matrix, null, null, null, false);
    #if !flash
    matrix.tx -= matrix.a*0.5;
    target.draw(source, matrix, null, null, null, false);
    matrix.tx += matrix.a*0.5;
    matrix.ty -= matrix.d*0.5;
    target.draw(source, matrix, null, null, null, false);
    matrix.tx -= matrix.a*0.5;
    target.draw(source, matrix, null, null, null, false);
    #end
    /*var x1:Int = Std.int(jts.moveX); var x2:Int = Std.int(jts.moveX + jts.scaleX*source.width);
    var y1:Int = Std.int(jts.moveY); var y2:Int = Std.int(jts.moveY + jts.scaleY*source.height);
    target.lock();
    for(xx in Main.imax(x1,0)...Main.imin(x2,target.width)) {
      for(yy in Main.imax(y1,0)...Main.imin(y2,target.height)) {
        target.setPixel32(xx, yy, PDraw.mergeOver(target.getPixel32(xx, yy),
            getPixelFrom(source, (xx-jts.moveX)/jts.scaleX, (yy-jts.moveY)/jts.scaleY)));
      }
    }
    target.unlock();*/
    return true;
  }
  public static function complexMatrixDraw(source:BitmapData, target:BitmapData, matrix:Matrix):Void {
    if(translateScaleDraw(source, target, justTranslateScale(matrix), matrix)) return;
    var ext:Rectangle = getExtents(matrix, source.width, source.height);
    matrix.invert();
    target.lock();
    for(xx in Main.imax(Std.int(ext.x),0)...Main.imin(Std.int(ext.x+ext.width+1),target.width)) {
      for(yy in Main.imax(Std.int(ext.y),0)...Main.imin(Std.int(ext.y+ext.height+1),target.height)) {
        _spoint.setTo(xx, yy);
        transformSPoint(matrix);
        target.setPixel32(xx, yy, PDraw.mergeOver(target.getPixel32(xx, yy),
            getPixelFrom(source, _spoint.x, _spoint.y)));
      }
    }
    target.unlock();
  }
  public static function getExtents(matrix:Matrix, width:Int, height:Int):Rectangle {
    if(_spoint == null) _spoint = new Point();
    if(_srect == null) _srect = new Rectangle();
    _spoint.setTo(0, 0);
    transformSPoint(matrix);
    var l:Float = _spoint.x, r:Float = l,
      u:Float = _spoint.y, d:Float = u;
    var xys:Array<Float> = [width, 0, width, height, 0, height];
    for(i in 0...3) {
      _spoint.setTo(xys[i*2], xys[i*2+1]);
      transformSPoint(matrix);
      l = Math.min(l, _spoint.x); r = Math.max(r, _spoint.x);
      u = Math.min(u, _spoint.y); d = Math.max(d, _spoint.y);
    }
    _srect.setTo(l, u, r-l, d-u);
    return _srect;
  }
  private static inline function transformSPoint(mat:Matrix):Void {
    var t:Point = mat.transformPoint(_spoint);
    _spoint.x = t.x; _spoint.y = t.y;
  }
  public static inline function getPixelFrom(bmp:BitmapData, x:Float, y:Float):Int {
    return bmp.getPixel32(Std.int(x), Std.int(y));
  }
  public static function setPixelIn(bmp:BitmapData, x:Float, y:Float, color:Int):Bool {
    bmp.setPixel32(Std.int(x), Std.int(y), color);
    return x >= 0 && y >= 0 && x < bmp.width && y < bmp.height;
  }
  public function clone():SymbolLayer {
    return new SymbolLayer(sym, x, y, _width, _height);
  }
  public function getDrawn(forceRedraw:Null<Bool> = null):BitmapData {
    var forceRedraw2:Bool = false;
    if(forceRedraw == null) {
      forceRedraw2 = !_lastRender.unchanged(this);
    } else {
      forceRedraw2 = forceRedraw;
    }
    if(forceRedraw2) {
      var prefWidth:Int = Std.int(_width*scaleX);
      var prefHeight:Int = Std.int(_height*scaleY);
      if(_rendered.width != prefWidth || _rendered.height != prefHeight) {
        _rendered.dispose();
        _rendered = new BitmapData(prefWidth, prefHeight, true, 0);
      }
      _rendered.fillRect(_rendered.rect, 0);
      drawB(_rendered, -x, -y, true);
      _lastRender.capture(this);
    }
    return _rendered;
  }
  public function getVersion():Int {
    if(_lastRender.unchanged(this)) {
      return _version;
    } else {
      return ++_version;
    }
  }
  public inline function getSimpleRender():Bool {
    return (angle % 360) == 0 && scaleX == 1 && scaleY == 1;
  }
  public inline function getMatrix():Matrix {
    _matrix.b = _matrix.c = 0;
    _matrix.a = scaleX;
    _matrix.d = scaleY;
    _matrix.tx = -sym.originX * scaleX;
    _matrix.ty = -sym.originY * scaleY;
    if(angle != 0) _matrix.rotate(angle * HXP.RAD);
    _matrix.tx += sym.originX + _point3.x;
    _matrix.ty += sym.originY + _point3.y;
    return _matrix;
  }
  public function setPixel(x:Int, y:Int, color:Int):Bool {
    if(getSimpleRender()) {
      return sym.setPixel(Std.int(x - symX), Std.int(y - symY), color);
    } else {
      _matrix = getMatrix();
      _matrix.invert();
      _point.setTo(x, y);
      var t:Point = _matrix.transformPoint(_point);
      _point.x = t.x; _point.y = t.y;
      return sym.setPixel(Std.int(_point.x), Std.int(_point.y), color);
    }
  }
  public function getPixel(x:Int, y:Int):Int {
    if(getSimpleRender()) {
      return sym.getPixel(Std.int(x - symX), Std.int(y - symY));
    } else {
      _matrix = getMatrix();
      _matrix.invert();
      _point.setTo(x, y);
      var t:Point = _matrix.transformPoint(_point);
      _point.x = t.x; _point.y = t.y;
      return sym.getPixel(Std.int(_point.x), Std.int(_point.y));
    }
  }
  public function floodFill(x:Int, y:Int, color:Int):Void {
    if(getSimpleRender()) {
      sym.floodFill(Std.int(x - symX), Std.int(y - symY), color);
    } else {
      _matrix = getMatrix();
      _matrix.invert();
      _point.setTo(x, y);
      var t:Point = _matrix.transformPoint(_point);
      _point.x = t.x; _point.y = t.y;
      sym.floodFill(Std.int(_point.x), Std.int(_point.y), color);
    }
  }
}
class JustTranslateScale {
  public var yes:Bool;
  public var scaleX:Float;
  public var scaleY:Float;
  public var moveX:Float;
  public var moveY:Float;
  public function new(yes:Bool = false, scaleX:Float = 1, scaleY:Float = 1, moveX:Float = 0, moveY:Float = 0) {
    this.yes = yes;
    this.scaleX = scaleX;
    this.scaleY = scaleY;
    this.moveX = moveX;
    this.moveY = moveY;
  }
}
