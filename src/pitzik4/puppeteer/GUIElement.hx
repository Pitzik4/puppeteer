/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.HXP;
import com.haxepunk.Entity;
import com.haxepunk.Graphic;
import com.haxepunk.Mask;
import pitzik4.gfx.Cursor;

class GUIElement extends Entity {
  public var owner:EditorScene;
  public var mouseOver:Bool = false;
  public var dead:Bool = false;
  
  public function new(x:Float = 0, y:Float = 0, graphic:Graphic = null, mask:Mask = null) {
    super(x, y, graphic, mask);
  }
  
  public function overlaps(mx:Int, my:Int):Bool {
    return mx >= x && mx < x + width && my >= y && my < y + height;
  }
  public function doCursor(curs:Cursor, mx:Int = 0, my:Int = 0):Void {
    curs.hidden = false; curs.state = CursorState.POINTER;
  }
  override public function update():Void {
    super.update();
    mouseOver = false;
  }
  public function focus():Bool { return true; }
  public function unfocus():Void {  }
}
