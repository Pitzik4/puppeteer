/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import com.haxepunk.HXP;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import flash.geom.Point;

class PDraw {
  private static var _rect:Rectangle;
  private static var _point:Point;
  
  
  public static function line(x1:Int, y1:Int, x2:Int, y2:Int, color:Int = 0xFFFFFF, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    color |= 0xFF000000;
    // get the drawing difference
    var X:Float = Math.abs(x2 - x1),
      Y:Float = Math.abs(y2 - y1),
      xx:Int,
      yy:Int;

    // draw a single pixel
    if (X == 0) {
      if (Y == 0) {
        target.setPixel32(x1, y1, color);
        return;
      }
      // draw a straight vertical line
      yy = y2 > y1 ? 1 : -1;
      while (y1 != y2) {
        target.setPixel32(x1, y1, color);
        y1 += yy;
      }
      target.setPixel32(x2, y2, color);
      return;
    }

    if (Y == 0) {
      // draw a straight horizontal line
      xx = x2 > x1 ? 1 : -1;
      while (x1 != x2) {
        target.setPixel32(x1, y1, color);
        x1 += xx;
      }
      target.setPixel32(x2, y2, color);
      return;
    }

    xx = x2 > x1 ? 1 : -1;
    yy = y2 > y1 ? 1 : -1;
    var c:Float = 0,
      slope:Float;

    if (X > Y) {
      slope = Y / X;
      c = .5;
      while (x1 != x2) {
        target.setPixel32(x1, y1, color);
        x1 += xx;
        c += slope;
        if (c >= 1) {
          y1 += yy;
          c -= 1;
        }
      }
      target.setPixel32(x2, y2, color);
    }
    else {
      slope = X / Y;
      c = .5;
      while (y1 != y2) {
        target.setPixel32(x1, y1, color);
        y1 += yy;
        c += slope;
        if (c >= 1) {
          x1 += xx;
          c -= 1;
        }
      }
      target.setPixel32(x2, y2, color);
    }
  }
  
  public static function ellipse(x:Int, y:Int, width:Int, height:Int, color:Int = 0xFFFFFF, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    color |= 0xFF000000;
    if(width == 0 || height == 0) return;
    if(width == 1 || height == 1) {
      line(x, y, x+width-1, y+height-1, color, target);
      return;
    }
    target.lock();
    var radius:Int = Std.int(Math.max(width, height)*0.5);
    var diameter:Int = radius * 2 + 1;
    var diam2:Float = diameter * 0.5;
    var xScale:Float = width / diameter; var yScale:Float = height / diameter;
    var f:Int = 1 - radius,
      fx:Int = 1,
      fy:Int = -2 * radius,
      xx:Int = 0,
      yy:Int = radius;
    setPixPlus(target, color, 0, radius, xScale, yScale, diam2, x, y);
    setPixPlus(target, color, 0, -radius, xScale, yScale, diam2, x, y);
    setPixPlus(target, color, radius, 0, xScale, yScale, diam2, x, y);
    setPixPlus(target, color, -radius, 0, xScale, yScale, diam2, x, y);
    while (xx < yy) {
      if (f >= 0) {
        yy --;
        fy += 2;
        f += fy;
      }
      xx ++;
      fx += 2;
      f += fx;
      setPixPlus(target, color, xx, yy, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, -xx, yy, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, xx, -yy, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, -xx, -yy, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, yy, xx, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, -yy, xx, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, yy, -xx, xScale, yScale, diam2, x, y);
      setPixPlus(target, color, -yy, -xx, xScale, yScale, diam2, x, y);
    }
    target.unlock();
  }
  public static inline function setPixPlus(target:BitmapData, color:Int, x:Float, y:Float, xScale:Float = 1, yScale:Float = 1, offs:Float = 0, xTrans:Float = 0, yTrans:Float = 0) {
    target.setPixel32(Std.int((x+offs)*xScale+xTrans), Std.int((y+offs)*yScale+yTrans), color);
  }
  public static function fillEllipse(x:Int, y:Int, width:Int, height:Int, color:Int = 0xFFFFFF, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    x--; y--;
    color |= 0xFF000000;
    if(width == 0 || height == 0) return;
    if(width == 1 || height == 1) {
      line(x, y, x+width-1, y+height-1, color, target);
      return;
    }
    target.lock();
    var xrad:Int = Std.int(width*0.5+0.5); var yrad:Int = Std.int(height*0.5+0.5);
    var xrad2:Int = xrad*xrad; var yrad2:Int = yrad*yrad;
    for(xx0 in -xrad...xrad+1) {
      for(yy0 in -yrad...yrad+1) {
        var xx:Int = xx0 - ((xx0>0)?(width+1)%2:0); var yy:Int = yy0 - ((yy0>0)?(height+1)%2:0);
        if((xx*xx+0.25)/xrad2+(yy*yy+0.25)/yrad2 <= 1) {
          target.setPixel32(xx0+x+xrad, yy0+y+yrad, color);
        }
      }
    }
    target.unlock();
  }
  /*public static function ellipse(x:Int, y:Int, width:Int, height:Int, color:Int = 0xFFFFFF, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    var xr:Float = width*0.5; var yr:Float = height*0.5;
    var xc:Float = x + xr;    var yc:Float = y + yr;
    var last:Int = 0;
    for(xxi in 1...width+1) {
      var xx:Float = xxi-xr;
      var h:Int = Std.int(Math.sqrt(xr*xr-xx*xx)*yr/xr+0.5);
      line(x+xxi-1, Std.int(yc+last)-(height+1)%2, x+xxi, Std.int(yc+h)-(height+1)%2, color, target);
      line(x+xxi-1, Std.int(yc-last), x+xxi, Std.int(yc-h), color, target);
      last = h;
    }
  }*/
  
  public static function rect(x:Float, y:Float, width:Float, height:Float, color:Int = 0xFFFFFF, thick:Int = 1, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    color |= 0xFF000000;
    if(_rect == null) _rect = new Rectangle();
    _rect.x = x; _rect.y = y; _rect.width = width; _rect.height = thick;
    target.fillRect(_rect, color);
    _rect.y = y + height - thick;
    target.fillRect(_rect, color);
    _rect.y = y + thick; _rect.width = thick; _rect.height = height - thick*2;
    target.fillRect(_rect, color);
    _rect.x = x + width - thick;
    target.fillRect(_rect, color);
  }
  public static function fillRect(x:Float, y:Float, width:Float, height:Float, color:Int = 0xFFFFFF, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    color |= 0xFF000000;
    if(_rect == null) _rect = new Rectangle();
    _rect.x = x; _rect.y = y; _rect.width = width; _rect.height = height;
    target.fillRect(_rect, color);
  }
  
  public static inline function drawImage(source:BitmapData, x:Float = 0, y:Float = 0, target:BitmapData = null):Void {
    if(target == null)
      target = HXP.buffer;
    if(_point == null) _point = new Point();
    _point.setTo(x, y);
    target.copyPixels(source, source.rect, _point, null, null, true);
  }
  
  public static function mergeOver(bottom:Int, top:Int):Int {
    var aA:Int = top >> 24;
    if(aA < 0) aA += 0x100;
    if(aA != 0) {
      if(aA != 0xFF) {
        var rA = (top & 0xFF0000) >> 16,
          gA = (top & 0xFF00) >> 8, bA = top & 0xFF,
          aB = bottom >> 24, rB = (bottom & 0xFF0000) >> 16,
          gB = (bottom & 0xFF00) >> 8, bB = bottom & 0xFF;
        if(aB < 0) aB += 0x100;
        var rOut:Int = Std.int((rA * aA / 255) + (rB * aB * (255 - aA) / (255*255))),
          gOut:Int = Std.int((gA * aA / 255) + (gB * aB * (255 - aA) / (255*255))),
          bOut:Int = Std.int((bA * aA / 255) + (bB * aB * (255 - aA) / (255*255))),
          aOut:Int = Std.int(aA + (aB * (255 - aA) / 255));
        return aOut << 24 & rOut << 16 & gOut << 8 & bOut;
      } else {
        return top;
      }
    } else {
      return bottom;
    }
  }
  
  public static function genCircle(diameter:Int, color:Int = 0xFFFFFF):BitmapData {
    var out:BitmapData = new BitmapData(diameter, diameter, true, 0);
    PDraw.ellipse(0, 0, diameter, diameter, color, out);
    return out;
  }
}
