/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pitzik4.puppeteer;

import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.Rectangle;
import flash.geom.Point;
import flash.geom.Matrix;
import com.haxepunk.HXP;

class Symbol {
  public var originX:Int;
  public var originY:Int;
  public var layers:Array<ILayer>;
  private var layerVersions:Array<Int>;
  public var width:Int;
  public var height:Int;
  private var _rendered:BitmapData;
  private var _version:Int = 1;
  private var _lastOriginX:Int = 0;
  private var _lastOriginY:Int = 0;
  private var _lastWidth:Int = 1;
  private var _lastHeight:Int = 1;
  private var _dirty:Bool = false;
  private var _lastVersion:Int = 0;
  private var _bmp:BitmapData;
  
  public function new(?source:Dynamic, originX:Int = 0, originY:Int = 0, copy:Bool = true) {
    this.originX = this.originY = 0;
    if(source == null) {
      layers = [new BitmapLayer(32, 32)];
      width = height = 32;
    } else if(Std.is(source, BitmapData)) {
      var bml = new BitmapLayer(0, 0, cast(source, BitmapData), copy);
      layers = [bml];
      width = bml.layerWidth; height = bml.layerHeight;
    } else if(Std.is(source, Rectangle)) {
      var bml = new BitmapLayer(Std.int(source.width), Std.int(source.height));
      layers = [bml];
      originX = -Std.int(source.x); originY = -Std.int(source.y);
      width = bml.layerWidth; height = bml.layerHeight;
    } else if(Std.is(source, Point)) {
      var bml = new BitmapLayer(Std.int(source.x), Std.int(source.y));
      layers = [bml];
      width = bml.layerWidth; height = bml.layerHeight;
    } else if(Std.is(source, Symbol)) {
      var symsrc:Symbol = cast(source, Symbol);
      layers = [];
      for(l in symsrc.layers) {
        layers.push(l.clone());
      }
      originX = symsrc.originX; originY = symsrc.originY;
      width = symsrc.width; height = symsrc.height;
    }
    layerVersions = [];
    for(i in 0...layers.length) {
      layerVersions.push(0);
    }
    _rendered = new BitmapData(width, height, true, 0);
    this.originX += originX; this.originY += originY;
    _dirty = true;
  }
  
  public function expand(left:Int = 0, right:Int = 0, up:Int = 0, down:Int = 0):Void {
    for(l in layers) {
      l.expand(left, right, up, down);
    }
    width += left + right; height += up + down;
    _rendered = new BitmapData(width, height, true, 0);
    originX += left; originY += up;
    _dirty = true;
  }
  public function resize(width:Int, height:Int):Void {
    if(width == this.width && height == this.height) return;
    for(l in layers) {
      l.resize(width, height);
    }
    this.width = width; this.height = height;
    _rendered = new BitmapData(width, height, true, 0);
    _dirty = true;
  }
  public function merge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
    makeEditableTop();
    layers[0].merge(source, x + originX, y + originY);
    _dirty = true;
  }
  public function eraseMerge(source:BitmapData, x:Float = 0, y:Float = 0):Void {
    x += originX; y += originY;
    for(l in layers) {
      l.eraseMerge(source, x, y);
    }
    _dirty = true;
  }
  public function draw(target:BitmapData, x:Float = 0, y:Float = 0):Void {
    for(i in 1...layers.length+1) {
      layers[layers.length-i].draw(target, x - originX, y - originY);
    }
  }
  public function addLayer(layer:ILayer, pos:Int = 0):Symbol {
    layers.insert(pos, layer);
    layerVersions.insert(pos, 0);
    _dirty = true;
    var modX:Int = width - layer.layerWidth,
      modY:Int = height - layer.layerHeight,
      modX2:Int = Std.int(modX*0.5),
      modY2:Int = Std.int(modY*0.5);
    if(modX != 0 || modY != 0)
      layer.expand(modX2, modY2, modX-modX2, modY-modY2);
    return this;
  }
  public function removeLayerNumber(pos:Int = 0):ILayer {
    var l:ILayer = layers[pos];
    layers.splice(pos, 1);
    layerVersions.splice(pos, 1);
    _dirty = true;
    return l;
  }
  public function removeLayer(layer:ILayer):Bool {
    var pos:Int = Lambda.indexOf(layers, layer);
    _dirty = true;
    if(pos >= 0) {
      removeLayerNumber(pos);
      return true;
    }
    return false;
  }
  public function rendered():BitmapData {
    _rendered.fillRect(_rendered.rect, 0);
    draw(_rendered, originX, originY);
    return _rendered;
  }
  public function getModified():Bool {
    if(_dirty) return true;
    if(originX != _lastOriginX || originY != _lastOriginY
          || width != _lastWidth || height != _lastHeight) {
      return true;
    }
    for(i in 0...layers.length) {
      if(layers[i].getVersion() != layerVersions[i]) {
        return true;
      }
    }
    return false;
  }
  public function getVersion():Int {
    if(getModified()) {
      _lastOriginX = originX; _lastOriginY = originY;
      _lastWidth = width; _lastHeight = height;
      for(i in 0...layers.length) {
        layerVersions[i] = layers[i].getVersion();
      }
      _dirty = false;
      return ++_version;
    } else {
      return _version;
    }
  }
  public function setPixel(x:Int, y:Int, color:Int):Bool {
    _dirty = true;
    makeEditableTop();
    return layers[0].setPixel(x + originX, y + originY, color);
  }
  public function getPixel(x:Int, y:Int):Int {
    x += originX; y += originY;
    var col:Int = 0;
    for(i in 1...layers.length+1) {
      col = PDraw.mergeOver(col, layers[layers.length-i].getPixel(x, y));
    }
    return col;
  }
  public function makeEditableTop():BitmapLayer {
    var out:BitmapLayer = null;
    if(layers.length <= 0 || !Std.is(layers[0], BitmapLayer)) {
      addLayer(out = new BitmapLayer(width, height));
    } else {
      out = cast(layers[0], BitmapLayer);
    }
    return out;
  }
  public function clone():Symbol {
    return new Symbol(this);
  }
  public function getImage():BitmapData {
    var vers:Int = _lastVersion;
    _lastVersion = getVersion();
    if(vers != _lastVersion) {
      return rendered();
    } else {
      return _rendered;
    }
  }
  public function floodFill(x:Int, y:Int, color:Int):Void {
    if(_bmp == null || _bmp.width != getImage().width || _bmp.height != _rendered.height) {
      _bmp = new BitmapData(_rendered.width, _rendered.height, true, 0);
    } else {
      _bmp.fillRect(_bmp.rect, 0);
    }
    _bmp.threshold(_rendered, _rendered.rect, HXP.zero, "==", _rendered.getPixel32(x, y), ColorBoxes.isBright(color)?0xFF000000:0xFFFFFFFF);
    _bmp.floodFill(x, y, color);
    _bmp.threshold(_bmp, _bmp.rect, HXP.zero, "!=", color);
    merge(_bmp);
  }
}
