package pitzik4.gfx;

import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;
import com.haxepunk.graphics.atlas.Atlas;
import com.haxepunk.graphics.atlas.AtlasRegion;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;

class BGColor extends Graphic {
  private var _rect:Rectangle;
  
  public function new() {
    super();
    _rect = new Rectangle();
    blit = (HXP.renderMode != RenderMode.HARDWARE);
  }
  
  override public function render(target:BitmapData, point:Point, camera:Point):Void {
    _rect.x = _rect.y = 0;
    _rect.width = target.width; _rect.height = target.height;
    target.fillRect(_rect, HXP.stage.color | 0xFF000000);
  }
}
