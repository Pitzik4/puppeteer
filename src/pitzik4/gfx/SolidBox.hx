package pitzik4.gfx;

import com.haxepunk.Graphic;
import com.haxepunk.HXP;
import com.haxepunk.RenderMode;
import com.haxepunk.graphics.atlas.Atlas;
import com.haxepunk.graphics.atlas.AtlasRegion;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;

class SolidBox extends Graphic {
  public static var region:AtlasRegion;
  public static inline var WHITE:Int = 0xFFFFFFFF;
  
  private var _rect:Rectangle;
  
  public var width:Float;
  public var height:Float;
  private var _color:Int = WHITE;
  private var _red:Float = 1.0;
  private var _green:Float = 1.0;
  private var _blue:Float = 1.0;
  public var color(get, set):Int;
  public function get_color():Int { return _color; }
  public function set_color(value:Int):Int {
    value |= 0xFF000000;
    if (_color == value) return value;
    _color = value;
#if !flash
    if(!blit) {
      _red = HXP.getRed(_color) / 255.0;
      _green = HXP.getGreen(_color) / 255.0;
      _blue = HXP.getBlue(_color) / 255.0;
    }
#end
    return _color;
  }
  
  public function new(color:Int = WHITE, x:Float = 0, y:Float = 0, width:Float = 32, height:Float = 32) {
    super();
    _rect = new Rectangle();
    this.x = x; this.y = y;
    this.width = width; this.height = height;
    this.color = color;
    
    blit = (HXP.renderMode != RenderMode.HARDWARE);
    if(!blit && region == null) {
      region = Atlas.loadImageAsRegion(HXP.createBitmap(1,1,false,WHITE));
    }
  }
  
  override public function render(target:BitmapData, point:Point, camera:Point):Void {
    _rect.x = point.x+x-camera.x*scrollX; _rect.y = point.y+y-camera.y*scrollY; _rect.width = width; _rect.height = height;
    if(width > 0 && height > 0)
      target.fillRect(_rect, color);
  }
  override public function renderAtlas(layer:Int, point:Point, camera:Point):Void {
    var ww:Int = HXP.width; var wh:Int = HXP.height;
    var sx = HXP.screen.fullScaleX, sy = HXP.screen.fullScaleY;
    _rect.x = point.x+x-camera.x*scrollX; _rect.y = point.y+y-camera.y*scrollY; _rect.width = width; _rect.height = height;
    if(_rect.width > 0)
      region.draw(Math.floor(_rect.x * sx), Math.floor(_rect.y * sy), layer, _rect.width*sx, _rect.height*sy, 0, _red, _green, _blue);
  }
  
  public function overlaps(x:Float, y:Float, mx:Float, my:Float):Bool {
    x -= mx + this.x; y -= my + this.y;
    return x >= 0 && y >= 0 && x < width && y < height;
  }
}
