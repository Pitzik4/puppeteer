package pitzik4.gfx;

import com.haxepunk.Entity;
import com.haxepunk.graphics.Spritemap;
import com.haxepunk.utils.Input;
import com.haxepunk.Graphic;
import flash.display.BitmapData;
import flash.geom.Point;

class Cursor extends Entity {
  public var cursor:Spritemap;
  public var hidden(default, set):Bool;
  public function set_hidden(val:Bool):Bool {
    //if(hidden == val) return hidden;
    cursor.visible = cross.visible = !val;
    return hidden = val;
  }
  public var cross:InvertCrosshairs;
  public var state(default, set):CursorState;
  public function set_state(s:CursorState):CursorState {
    hidden = false;
    switch (s) {
      case CursorState.POINTER: graphic = cursor; cursor.play("pointer");
      case CursorState.TEXT: graphic = cursor; cursor.play("text");
      case CursorState.DROPPER: graphic = cursor; cursor.play("dropper");
      case CursorState.CROSSHAIR: graphic = cross;
    }
    return state = s;
  }
  
  public function new(source:Dynamic, offsetX:Int = 0, offsetY:Int = 0) {
    super();
    graphic = cursor = new Spritemap(source, 12, 18);
    cursor.x = -2; cursor.y = -6;
    cursor.add("pointer", [0]);
    cursor.add("text", [1]);
    cursor.add("dropper", [2]);
    cross = new InvertCrosshairs();
    layer = -16;
    state = CursorState.POINTER;
  }
  
  override public function update():Void {
    x = Input.mouseX; y = Input.mouseY;
  }
}
class InvertCrosshairs extends Graphic {
  public function new() {
    super();
    blit = true;
  }
  override public function render(target:BitmapData, point:Point, camera:Point) {
    var mx:Int = Std.int(point.x);
    var my:Int = Std.int(point.y);
    invertPixel(mx-3, my, target); invertPixel(mx-2, my, target);
    invertPixel(mx+3, my, target); invertPixel(mx+2, my, target);
    invertPixel(mx, my-3, target); invertPixel(mx, my-2, target);
    invertPixel(mx, my+3, target); invertPixel(mx, my+2, target);
    /*if(puppet.scale > 1) {
      invertPixel(mx, my, target);
    }*/
  }
  public static function invertPixel(x:Int, y:Int, target:BitmapData):Void {
    target.setPixel(x, y, ~target.getPixel(x, y));
  }
}
enum CursorState {
  POINTER; TEXT; DROPPER; CROSSHAIR;
}
