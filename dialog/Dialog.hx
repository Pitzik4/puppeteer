/* 
 * Copyright 2014 Pitzik4 (http://www.pitzik4.net/)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import systools.Dialogs;

class Dialog {
  public static function main():Void {
    var args = Sys.args();
    if(args.length < 1) {
      Dialogs.message("Nope.", "This file doesn't do anything on its own.", false);
      Sys.exit(0);
    }
    if(args[0] == "save") {
      Sys.exit(save(args));
    } else if(args[0] == "open") {
      Sys.exit(open(args));
    }
  }
  public static function save(args:Array<String>):Int {
    var title:String = args.length<2?"Save":args[1];
    var msg:String = args.length<3?"Save a file.":args[2];
    var start:String = args.length<4?getHomeDir():args[3];
    var out:String = Dialogs.saveFile(title, msg, start);
    if(out != null) Sys.print(out);
    return 0;
  }
  public static function open(args:Array<String>):Int {
    var title:String = args.length<2?"Open":args[1];
    var msg:String = args.length<3?"Open a file.":args[2];
    var count:Int = 1;
    var desc:Array<String> = ["All files"];
    var ext:Array<String> = ["*"];
    if(args.length > 3) {
      count = Std.parseInt(args[3]);
      if(args.length < 4+count*2) {
        return 1;
      }
      desc = []; ext = [];
      for(i in 0...count) {
        desc[i] = args[4+i*2];
        ext[i] = args[5+i*2];
      }
    }
    var out:Array<String> = Dialogs.openFile(title, msg, { count: count, descriptions: desc, extensions: ext });
    if(out == null) return 0;
    for(f in out) {
      Sys.println(f);
    }
    return 0;
  }
  public static function getHomeDir():String {
    for(e in ["HOMEPATH", "HOME"]) {
      var d:String = Sys.getEnv(e);
      if(d != null) {
        return d;
      }
    }
    return Sys.getCwd();
  }
}
